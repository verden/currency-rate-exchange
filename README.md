# Currency Rate exchange application


## Setup

Application provides docker compose file, to start please execute following command in application directory
```bash
docker-compose up
```

This command will create and start application in your local host with 8080 port opened.
In application used Java 11 version and Postgres database version 13.1

## API
API base path is
```bash 
http://localhost:8080/api/v1/{*}
```
To get detail api details please open
```bash
http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config
``` 
swagger url.

## Currency rate providers URL's
Application base currency is Euro
```bash
http://data.fixer.io/api/latest?access_key=4b0505f3444df9a924cd1dfb529699f1&format=1
https://v6.exchangerate-api.com/v6/689a047d219a8d7ef47a1a5f/latest/
```
## Login
Application has single admin user defined
#### Login - admin@cre.com
#### Password - 123

During the initial start of application command should be executed to
```bash
http://localhost:8080/api/v1/auth/login
```
URL to get JWT authentication token.
(sry for inconvenience, i could not find more simple way to configure it in swagger but you can find bellow the relevant command to execute from command line)

```bash
curl -i -X POST \
   -H "Content-Type:application/json" \
   -d \
'{
  "email": "admin@cre.com",
  "password": "123"
}' \
 'http://localhost:8080/api/v1/auth/login'
```
response of command execution will be similar to the following
```bash
{
"accessToken": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbkBjcmUuY29tIiwiZXhwIjoxNjEwNzI4MTEzfQ.GDxAQE3N__K55qvJPzhXDA0fAEhACdkBnmOhZP6ijgA",
"refreshToken": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbkBjcmUuY29tIiwiZXhwIjoxNjI2NTA2MzEzfQ.ukYOMlS0-EIDn9B1XcV1KGSpkV-osA2VSBHEKVrED9A",
"expiresAt": 1610728113000
}
```
Given accessToken should be passed as Swagger Authorize Bearer value.