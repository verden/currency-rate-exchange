package com.codebase.cre.converter;

import com.codebase.cre.common.exception.ApplicationGenericException;
import com.codebase.cre.converter.dto.ConvertRequest;
import com.codebase.cre.converter.dto.ConvertResponse;
import com.codebase.cre.converter.service.CurrencyConverterService;
import com.codebase.cre.converter.service.validator.CurrencyConversionValidator;
import com.codebase.cre.currency.entity.Currency;
import com.codebase.cre.currency.repository.CurrencyRepository;
import com.codebase.cre.currencyrate.entity.CurrencyRate;
import com.codebase.cre.currencyrate.repository.CurrencyRateRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author William Arustamyan
 */

public class CurrencyConverterServiceTest {

    private final CurrencyRateRepository currencyRateRepository = mock(CurrencyRateRepository.class);
    private final CurrencyRepository currencyRepository = mock(CurrencyRepository.class);

    private final CurrencyConversionValidator validator = new CurrencyConversionValidator(currencyRepository);
    private final CurrencyConverterService converterService = new CurrencyConverterService(currencyRateRepository, validator);

    private static Currency amd;
    private static Currency usd;

    private static CurrencyRate amdRate;
    private static CurrencyRate usdRate;

    @BeforeAll
    static void beforeAll() {
        setupCurrenciesAndRates();
    }

    @Test
    public void testConvertCurrenciesSuccess() {
        ConvertRequest request = new ConvertRequest();
        request.setFrom("AMD");
        request.setTo("USD");
        request.setAmount(2000d);

        when(currencyRepository.findByCode("AMD")).thenReturn(Optional.of(amd));
        when(currencyRepository.findByCode("USD")).thenReturn(Optional.of(usd));

        when(currencyRateRepository.findLatestCurrencyRate(amd.getId())).thenReturn(Optional.of(amdRate));
        when(currencyRateRepository.findLatestCurrencyRate(usd.getId())).thenReturn(Optional.of(usdRate));

        ConvertResponse convertResponse = converterService.convert(request);

        assertThat(convertResponse).isNotNull();
        assertThat(convertResponse.getFrom()).isEqualTo(request.getFrom());
        assertThat(convertResponse.getTo()).isEqualTo(request.getTo());
        assertThat(convertResponse.getAmount()).isEqualTo(request.getAmount());
        assertThat(convertResponse.getResult()).isEqualTo(800d);
    }

    @Test
    public void testConvertCurrenciesWithZeroValue() {
        ConvertRequest request = new ConvertRequest();
        request.setFrom("AMD");
        request.setTo("USD");
        request.setAmount(0d);

        when(currencyRepository.findByCode("AMD")).thenReturn(Optional.of(amd));
        when(currencyRepository.findByCode("USD")).thenReturn(Optional.of(usd));

        when(currencyRateRepository.findLatestCurrencyRate(amd.getId())).thenReturn(Optional.of(amdRate));
        when(currencyRateRepository.findLatestCurrencyRate(usd.getId())).thenReturn(Optional.of(usdRate));

        ConvertResponse convertResponse = converterService.convert(request);

        assertThat(convertResponse).isNotNull();
        assertThat(convertResponse.getFrom()).isEqualTo(request.getFrom());
        assertThat(convertResponse.getTo()).isEqualTo(request.getTo());
        assertThat(convertResponse.getAmount()).isEqualTo(request.getAmount());
        assertThat(convertResponse.getResult()).isEqualTo(0d);
    }

    @Test
    public void testConvertCurrenciesWithNegativeValueShouldFail() {
        ConvertRequest request = new ConvertRequest();
        request.setFrom("AMD");
        request.setTo("USD");
        request.setAmount(-1d);
        assertThrows(ApplicationGenericException.class, () -> converterService.convert(request));
    }


    @Test
    public void testConvertSameCurrenciesSuccess() {
        ConvertRequest request = new ConvertRequest();
        request.setFrom("AMD");
        request.setTo("AMD");
        request.setAmount(2000d);

        when(currencyRepository.findByCode("AMD")).thenReturn(Optional.of(amd));
        when(currencyRepository.findByCode("AMD")).thenReturn(Optional.of(amd));

        when(currencyRateRepository.findLatestCurrencyRate(amd.getId())).thenReturn(Optional.of(amdRate));
        when(currencyRateRepository.findLatestCurrencyRate(amd.getId())).thenReturn(Optional.of(amdRate));

        ConvertResponse convertResponse = converterService.convert(request);

        assertThat(convertResponse).isNotNull();
        assertThat(convertResponse.getFrom()).isEqualTo(request.getFrom());
        assertThat(convertResponse.getTo()).isEqualTo(request.getTo());
        assertThat(convertResponse.getAmount()).isEqualTo(request.getAmount());
        assertThat(convertResponse.getResult()).isEqualTo(1d);
    }

    @Test
    public void testConvertCurrenciesWithNonExistingCodeShouldFail() {
        when(currencyRepository.findByCode(RandomStringUtils.randomAlphabetic(3 + 1))).thenReturn(Optional.empty());

        ConvertRequest request = new ConvertRequest();
        request.setAmount(RandomUtils.nextDouble());

        assertThrows(ApplicationGenericException.class, () -> converterService.convert(request));
    }

    @Test
    public void testConvertCurrenciesWhenRateNotPresent() {

        when(currencyRepository.findByCode("AMD")).thenReturn(Optional.of(amd));
        when(currencyRepository.findByCode("USD")).thenReturn(Optional.of(usd));

        when(currencyRateRepository.findLatestCurrencyRate(RandomUtils.nextLong())).thenReturn(Optional.empty());
        when(currencyRateRepository.findLatestCurrencyRate(amd.getId())).thenReturn(Optional.of(amdRate));

        ConvertRequest request = new ConvertRequest();
        request.setAmount(RandomUtils.nextDouble());
        assertThrows(ApplicationGenericException.class, () -> converterService.convert(request));

    }

    private static void setupCurrenciesAndRates() {
        /*base currency*/new Currency(1L, "EUR", "Euro");
        amd = new Currency(2L, "AMD", "Armenian Dram");
        usd = new Currency(3L, "USD", "United States Dollar");

        amdRate = new CurrencyRate(1L, amd, LocalDateTime.now(), 500d);
        usdRate = new CurrencyRate(2L, usd, LocalDateTime.now(), 200d);
    }
}
