package com.codebase.cre.currency;

import com.codebase.cre.currency.dto.CurrencyResponse;
import com.codebase.cre.currency.entity.Currency;
import com.codebase.cre.currency.mapper.CurrencyToResponseMapper;
import com.codebase.cre.currency.repository.CurrencyRepository;
import com.codebase.cre.currency.service.CurrencyService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author William Arustamyan
 */

public class CurrencyServiceTest {

    private final CurrencyRepository currencyRepository = mock(CurrencyRepository.class);
    private final CurrencyToResponseMapper currencyToResponseMapper = new CurrencyToResponseMapper();

    private final CurrencyService currencyService = new CurrencyService(currencyRepository, currencyToResponseMapper);

    private static Currency amd;
    private static Currency usd;

    @BeforeAll
    static void beforeAll() {
        setupCurrencies();
    }

    @Test
    public void testSelectAllCurrenciesSuccess() {
        when(currencyRepository.findAll()).thenReturn(List.of(amd, usd));
        List<CurrencyResponse> currencies = currencyService.currencies();

        assertThat(currencies).isNotNull();
        assertThat(currencies).isNotEmpty();

        assertThat(currencies.size()).isEqualTo(2);
        assertThat(currencies.get(0).getCode()).isEqualTo(amd.getCode());
        assertThat(currencies.get(1).getCode()).isEqualTo(usd.getCode());
    }

    private static void setupCurrencies() {
        amd = new Currency(1L, "AMD", "Armenian Dram");
        usd = new Currency(2L, "USD", "United States Dollar");
    }
}
