package com.codebase.cre.currencyrate.dto;

import com.codebase.cre.currency.dto.CurrencyResponse;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author William Arustamyan
 */

@Data
public class CurrencyRateResponse {

    private Long id;

    private CurrencyResponse currency;

    private Double rate;

    private LocalDateTime rateDate;
}
