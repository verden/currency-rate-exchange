package com.codebase.cre.currencyrate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author William Arustamyan
 */

@Data
@AllArgsConstructor
public class CurrencyRatesResponse {

    private String baseCode;

    private List<CurrencyRateResponse> items;
}
