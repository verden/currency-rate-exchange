package com.codebase.cre.currencyrate.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author William Arustamyan
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyRateCreateRequest {

    @NotNull
    private Long currencyId;

    @NotNull
    @Min(1)
    private Double rate;

    private LocalDateTime rateDate;

}
