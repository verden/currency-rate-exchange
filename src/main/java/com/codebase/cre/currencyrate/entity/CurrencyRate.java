package com.codebase.cre.currencyrate.entity;

import com.codebase.cre.currency.entity.Currency;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * @author William Arustamyan
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "t_currency_rates",
        uniqueConstraints = @UniqueConstraint(columnNames = {"currency_id", "rate_date"})
)
public class CurrencyRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @EqualsAndHashCode.Include
    @ManyToOne
    @JoinColumn(name = "currency_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Currency currency;

    @EqualsAndHashCode.Include
    @Column(name = "rate_date", nullable = false)
    private LocalDateTime rateDate;

    @EqualsAndHashCode.Include
    @Column(name = "rate", nullable = false)
    private Double currencyRate;
}
