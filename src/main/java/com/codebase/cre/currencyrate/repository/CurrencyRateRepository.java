package com.codebase.cre.currencyrate.repository;

import com.codebase.cre.currencyrate.entity.CurrencyRate;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author William Arustamyan
 */

public interface CurrencyRateRepository extends JpaRepository<CurrencyRate, Long> {

    default Optional<CurrencyRate> findLatestCurrencyRate(Long currencyId) {
        List<CurrencyRate> rates = findByCurrencyIdOrderByRateDateDesc(currencyId, PageRequest.of(0, 1));
        if (!rates.isEmpty()) {
            return Optional.of(rates.get(0));
        }
        return Optional.empty();
    }

    List<CurrencyRate> findByCurrencyIdOrderByRateDateDesc(Long currencyId, Pageable pageable);

}
