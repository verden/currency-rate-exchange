package com.codebase.cre.currencyrate.mapper;

import com.codebase.cre.currency.repository.CurrencyRepository;
import com.codebase.cre.currencyrate.dto.CurrencyRateCreateRequest;
import com.codebase.cre.currencyrate.entity.CurrencyRate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.function.Function;

/**
 * @author William Arustamyan
 */

@Component
public class RequestToCurrencyRateMapper implements Function<CurrencyRateCreateRequest, CurrencyRate> {

    private final CurrencyRepository currencyRepository;

    public RequestToCurrencyRateMapper(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public CurrencyRate apply(CurrencyRateCreateRequest createRequest) {
        CurrencyRate instance = new CurrencyRate();
        instance.setCurrency(currencyRepository.getOne(createRequest.getCurrencyId()));
        if (createRequest.getRateDate() == null) {
            instance.setRateDate(LocalDateTime.now());
        } else {
            instance.setRateDate(createRequest.getRateDate());
        }
        instance.setCurrencyRate(createRequest.getRate());
        return instance;
    }
}
