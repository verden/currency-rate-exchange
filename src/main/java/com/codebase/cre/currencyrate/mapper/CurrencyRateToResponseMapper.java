package com.codebase.cre.currencyrate.mapper;

import com.codebase.cre.currency.mapper.CurrencyToResponseMapper;
import com.codebase.cre.currencyrate.dto.CurrencyRateResponse;
import com.codebase.cre.currencyrate.entity.CurrencyRate;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * @author William Arustamyan
 */

@Component
public class CurrencyRateToResponseMapper implements Function<CurrencyRate, CurrencyRateResponse> {

    private final CurrencyToResponseMapper currencyToResponseMapper;

    public CurrencyRateToResponseMapper(CurrencyToResponseMapper currencyToResponseMapper) {
        this.currencyToResponseMapper = currencyToResponseMapper;
    }

    @Override
    public CurrencyRateResponse apply(CurrencyRate currencyRate) {
        final CurrencyRateResponse response = new CurrencyRateResponse();
        response.setCurrency(currencyToResponseMapper.apply(currencyRate.getCurrency()));
        response.setRate(currencyRate.getCurrencyRate());
        response.setRateDate(currencyRate.getRateDate());
        response.setId(currencyRate.getId());
        return response;
    }
}
