package com.codebase.cre.currencyrate.mapper;

import com.codebase.cre.currencyrate.dto.CurrencyRateCreateRequest;
import com.codebase.cre.currencyrate.dto.CurrencyRateUpdateRequest;
import com.codebase.cre.currencyrate.entity.CurrencyRate;
import org.springframework.stereotype.Component;

/**
 * @author William Arustamyan
 */

@Component
public class CurrencyRateActionHandler {

    private final RequestToCurrencyRateMapper requestToCurrencyRateMapper;
    private final CurrencyRateUpdateHandler currencyRateUpdateHandler;

    public CurrencyRateActionHandler(RequestToCurrencyRateMapper requestToCurrencyRateMapper, CurrencyRateUpdateHandler currencyRateUpdateHandler) {
        this.requestToCurrencyRateMapper = requestToCurrencyRateMapper;
        this.currencyRateUpdateHandler = currencyRateUpdateHandler;
    }

    public CurrencyRate create(CurrencyRateCreateRequest createRequest) {
        return requestToCurrencyRateMapper.apply(createRequest);
    }

    public CurrencyRate update(Long rateId, CurrencyRateUpdateRequest updateRequest) {
        return currencyRateUpdateHandler.apply(rateId, updateRequest);
    }
}
