package com.codebase.cre.currencyrate.mapper;

import com.codebase.cre.currency.repository.CurrencyRepository;
import com.codebase.cre.currencyrate.dto.CurrencyRateUpdateRequest;
import com.codebase.cre.currencyrate.entity.CurrencyRate;
import com.codebase.cre.currencyrate.repository.CurrencyRateRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.function.BiFunction;

/**
 * @author William Arustamyan
 */

@Component
public class CurrencyRateUpdateHandler implements BiFunction<Long, CurrencyRateUpdateRequest, CurrencyRate> {

    private final CurrencyRepository currencyRepository;
    private final CurrencyRateRepository currencyRateRepository;

    public CurrencyRateUpdateHandler(CurrencyRepository currencyRepository, CurrencyRateRepository currencyRateRepository) {
        this.currencyRepository = currencyRepository;
        this.currencyRateRepository = currencyRateRepository;
    }

    @Override
    public CurrencyRate apply(Long rateId, CurrencyRateUpdateRequest updateRequest) {
        final CurrencyRate rate = currencyRateRepository.getOne(rateId);
        rate.setCurrencyRate(updateRequest.getRate());
        if (updateRequest.getRateDate() != null) {
            rate.setRateDate(updateRequest.getRateDate());
        } else {
            rate.setRateDate(LocalDateTime.now());
        }

        if (!rate.getCurrency().getId().equals(updateRequest.getCurrencyId())) {
            rate.setCurrency(currencyRepository.getOne(updateRequest.getCurrencyId()));
        }
        return rate;
    }
}
