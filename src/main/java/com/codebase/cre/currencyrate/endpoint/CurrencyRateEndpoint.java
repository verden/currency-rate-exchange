package com.codebase.cre.currencyrate.endpoint;

import com.codebase.cre.currencyrate.dto.CurrencyRateCreateRequest;
import com.codebase.cre.currencyrate.dto.CurrencyRateResponse;
import com.codebase.cre.currencyrate.dto.CurrencyRateUpdateRequest;
import com.codebase.cre.currencyrate.dto.CurrencyRatesResponse;
import com.codebase.cre.currencyrate.service.CurrencyRateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author William Arustamyan
 */


@RestController
@RequestMapping("/api/v1/currency-rate")
public class CurrencyRateEndpoint {

    private final CurrencyRateService currencyRateService;

    public CurrencyRateEndpoint(CurrencyRateService currencyRateService) {
        this.currencyRateService = currencyRateService;
    }

    @GetMapping
    public CurrencyRatesResponse rates() {
        return currencyRateService.rates();
    }

    @Operation(summary = "Return list of last updated rates. Rates will be separated by provider source")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Return rates",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CurrencyRatesResponse.class))})})
    @GetMapping("/latest")
    public CurrencyRatesResponse latestRates() {
        return currencyRateService.latest();
    }


    @Operation(summary = "Add new Currency rate")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Currency rate successfully added",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CurrencyRatesResponse.class))}),
            @ApiResponse(responseCode = "400", description = "In case if creating base currency rate with wrong rate amount",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "In case if rate provider source not found",
                    content = @Content)})
    @PostMapping
    public CurrencyRateResponse add(@Valid @RequestBody CurrencyRateCreateRequest createRequest) {
        return currencyRateService.create(createRequest);
    }


    @Operation(summary = "Update existing currency rate")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Currency rate successfully added",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CurrencyRatesResponse.class))}),
            @ApiResponse(responseCode = "400", description = "In case if trying to update same currency rate with wrong rate amount",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "In case if updatable currency rate nut found, or rate source does not exist",
                    content = @Content)})
    @PutMapping("/{id}")
    public CurrencyRateResponse update(@PathVariable("id") Long id, @RequestBody CurrencyRateUpdateRequest updateRequest) {
        return currencyRateService.update(id, updateRequest);
    }

    @Operation(summary = "Delete existing currency")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Currency rate successfully deleted"),
            @ApiResponse(responseCode = "404", description = "In case if currency rate does not exist",
                    content = @Content)})
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCurrencyRate(@PathVariable("id") Long id) {
        currencyRateService.delete(id);
        return ResponseEntity.noContent()
                .build();
    }
}
