package com.codebase.cre.currencyrate.service.validator;

import com.codebase.cre.common.exception.ApplicationGenericException;
import com.codebase.cre.currency.repository.CurrencyRepository;
import com.codebase.cre.currencyrate.dto.CurrencyRateCreateRequest;
import com.codebase.cre.currencyrate.dto.CurrencyRateUpdateRequest;
import com.codebase.cre.currencyrate.repository.CurrencyRateRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author William Arustamyan
 */

@Component
@Transactional(readOnly = true)
public class CurrencyRateValidator {

    private final CurrencyRepository currencyRepository;
    private final CurrencyRateRepository currencyRateRepository;

    public CurrencyRateValidator(CurrencyRepository currencyRepository,
                                 CurrencyRateRepository currencyRateRepository) {
        this.currencyRepository = currencyRepository;
        this.currencyRateRepository = currencyRateRepository;
    }

    public void validateCurrencyRateCreation(CurrencyRateCreateRequest createRequest) {
        validateCurrencyExistence(createRequest.getCurrencyId());
        validateBaseCurrencyRate(createRequest.getCurrencyId(), createRequest.getRate());
    }

    public void validateCurrencyRateUpdate(Long id, CurrencyRateUpdateRequest updateRequest) {
        validateCurrencyRateExistence(id);
        validateCurrencyExistence(updateRequest.getCurrencyId());
        validateBaseCurrencyRate(id, updateRequest.getRate());
    }

    public void validateCurrencyRateDelete(Long id) {
        validateCurrencyRateExistence(id);
    }


    private void validateCurrencyExistence(Long currencyId) {
        if (!currencyRepository.existsById(currencyId)) {
            throw new ApplicationGenericException(HttpStatus.NOT_FOUND, "Currency with id : " + currencyId + " not found");
        }
    }

    private void validateBaseCurrencyRate(Long currencyId, Double rate) {
        if (currencyRepository.baseCurrency().getId().equals(currencyId) && rate != 1) {
            throw new ApplicationGenericException(HttpStatus.BAD_REQUEST, "Same currencies cannot have rate not equal to 1");
        }
    }

    private void validateCurrencyRateExistence(Long id) {
        if (!currencyRateRepository.existsById(id)) {
            throw new ApplicationGenericException(HttpStatus.NOT_FOUND, "Currency rate with id : " + id + " not found");
        }
    }
}
