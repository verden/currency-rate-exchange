package com.codebase.cre.currencyrate.service;

import com.codebase.cre.currency.repository.CurrencyRepository;
import com.codebase.cre.currencyrate.dto.CurrencyRateCreateRequest;
import com.codebase.cre.currencyrate.dto.CurrencyRateResponse;
import com.codebase.cre.currencyrate.dto.CurrencyRateUpdateRequest;
import com.codebase.cre.currencyrate.dto.CurrencyRatesResponse;
import com.codebase.cre.currencyrate.entity.CurrencyRate;
import com.codebase.cre.currencyrate.mapper.CurrencyRateActionHandler;
import com.codebase.cre.currencyrate.mapper.CurrencyRateToResponseMapper;
import com.codebase.cre.currencyrate.repository.CurrencyRateRepository;
import com.codebase.cre.currencyrate.service.validator.CurrencyRateValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author William Arustamyan
 */

@Service
@Transactional
public class CurrencyRateService {


    @Value("${cre.base.currency.code}")
    private String baseCurrencyCode;

    private final CurrencyRepository currencyRepository;
    private final CurrencyRateRepository currencyRateRepository;

    private final CurrencyRateValidator currencyRateValidator;
    private final CurrencyRateActionHandler rateActionHandler;
    private final CurrencyRateToResponseMapper rateToResponseMapper;


    public CurrencyRateService(CurrencyRepository currencyRepository,
                               CurrencyRateValidator rateValidator,
                               CurrencyRateActionHandler actionHandler,
                               CurrencyRateRepository currencyRateRepository,
                               CurrencyRateToResponseMapper rateToResponseMapper) {
        this.rateActionHandler = actionHandler;
        this.currencyRateValidator = rateValidator;
        this.currencyRepository = currencyRepository;
        this.rateToResponseMapper = rateToResponseMapper;
        this.currencyRateRepository = currencyRateRepository;
    }

    public CurrencyRateResponse create(CurrencyRateCreateRequest createRequest) {
        currencyRateValidator.validateCurrencyRateCreation(createRequest);
        return Optional.of(createRequest)
                .stream()
                .map(rateActionHandler::create)
                .map(currencyRateRepository::save)
                .map(rateToResponseMapper)
                .findFirst()
                .orElse(null);
    }

    public CurrencyRateResponse update(Long id, CurrencyRateUpdateRequest updateRequest) {
        currencyRateValidator.validateCurrencyRateUpdate(id, updateRequest);
        return Optional.of(currencyRateRepository.save(rateActionHandler.update(id, updateRequest))).stream()
                .map(rateToResponseMapper).findFirst().orElse(null);
    }

    @Transactional(readOnly = true)
    public CurrencyRatesResponse rates() {
        List<CurrencyRate> allRates = currencyRateRepository.findAll();
        return new CurrencyRatesResponse(
                baseCurrencyCode,
                allRates.stream()
                        .map(rateToResponseMapper)
                        .collect(Collectors.toList())
        );
    }

    @Transactional(readOnly = true)
    public CurrencyRatesResponse latest() {
        List<Long> currencyIds = currencyRepository.allIds();
        List<CurrencyRate> latestRates = new ArrayList<>();
        currencyIds.forEach(id -> currencyRateRepository.findLatestCurrencyRate(id).ifPresent(latestRates::add));
        List<CurrencyRateResponse> items = latestRates.stream()
                .map(rateToResponseMapper)
                .collect(Collectors.toList());
        return new CurrencyRatesResponse(baseCurrencyCode, items);
    }

    public void delete(Long id) {
        currencyRateValidator.validateCurrencyRateDelete(id);
        currencyRateRepository.deleteById(id);
    }


    public void create(List<CurrencyRateCreateRequest> createRequests) {
        createRequests.forEach(currencyRateValidator::validateCurrencyRateCreation);
        List<CurrencyRate> rates = createRequests.stream()
                .map(rateActionHandler::create)
                .collect(Collectors.toList());
        currencyRateRepository.saveAll(rates);
    }

}
