package com.codebase.cre.converter.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author William Arustamyan
 */

@Data
public class ConvertRequest {

    @NotEmpty
    private String from;

    @NotEmpty
    private String to;

    @NotNull
    @Min(1)
    private Double amount;
}
