package com.codebase.cre.converter.dto;

import lombok.Data;

/**
 * @author William Arustamyan
 */

@Data
public class ConvertResponse {

    private String from;

    private String to;

    private Double amount;

    private Double result;
}
