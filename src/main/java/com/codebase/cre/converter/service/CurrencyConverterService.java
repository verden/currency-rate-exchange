package com.codebase.cre.converter.service;

import com.codebase.cre.common.exception.ApplicationGenericException;
import com.codebase.cre.converter.dto.ConvertRequest;
import com.codebase.cre.converter.dto.ConvertResponse;
import com.codebase.cre.converter.service.validator.CurrencyConversionValidator;
import com.codebase.cre.currency.entity.Currency;
import com.codebase.cre.currencyrate.entity.CurrencyRate;
import com.codebase.cre.currencyrate.repository.CurrencyRateRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

/**
 * @author William Arustamyan
 */

@Service
@Transactional
public class CurrencyConverterService {

    private static final int SCALE = 4;

    private final CurrencyRateRepository currencyRateRepository;
    private final CurrencyConversionValidator conversionValidator;


    public CurrencyConverterService(CurrencyRateRepository currencyRateRepository,
                                    CurrencyConversionValidator conversionValidator) {
        this.conversionValidator = conversionValidator;
        this.currencyRateRepository = currencyRateRepository;
    }


    public ConvertResponse convert(ConvertRequest request) {
        conversionValidator.validateNegativeAmount(request.getAmount());
        final Currency fromCurrency = conversionValidator.validateAndGet(request.getFrom());
        final Currency toCurrency = conversionValidator.validateAndGet(request.getTo());
        if (fromCurrency.equals(toCurrency)) {
            return constructCurrencyConvertResponse(request, 1d);
        }
        Optional<CurrencyRate> rateForFrom = currencyRateRepository.findLatestCurrencyRate(fromCurrency.getId());
        Optional<CurrencyRate> rateForTo = currencyRateRepository.findLatestCurrencyRate(toCurrency.getId());
        if (rateForFrom.isEmpty() || rateForTo.isEmpty()) {
            throw new ApplicationGenericException(HttpStatus.NOT_FOUND, "Some rates not present.");
        }
        BigDecimal rateFrom = makeDecimal(rateForFrom.get().getCurrencyRate());
        BigDecimal rateTo = makeDecimal(rateForTo.get().getCurrencyRate());
        BigDecimal fromToBase = makeDecimal(request.getAmount()).divide(rateFrom, SCALE, RoundingMode.HALF_EVEN);
        BigDecimal result = fromToBase.multiply(rateTo).setScale(SCALE, RoundingMode.CEILING);
        return constructCurrencyConvertResponse(request, result.doubleValue());
    }

    private BigDecimal makeDecimal(Double value) {
        return new BigDecimal(value);
    }

    private ConvertResponse constructCurrencyConvertResponse(ConvertRequest request, Double result) {
        final ConvertResponse response = new ConvertResponse();
        response.setFrom(request.getFrom());
        response.setTo(request.getTo());
        response.setAmount(request.getAmount());
        response.setResult(result);
        return response;
    }
}
