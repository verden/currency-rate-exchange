package com.codebase.cre.converter.service.validator;

import com.codebase.cre.common.exception.ApplicationGenericException;
import com.codebase.cre.currency.entity.Currency;
import com.codebase.cre.currency.repository.CurrencyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author William Arustamyan
 */

@Service
@Transactional(readOnly = true)
public class CurrencyConversionValidator {

    private final CurrencyRepository currencyRepository;

    public CurrencyConversionValidator(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    public Currency validateAndGet(String currencyCode) {
        return currencyRepository.findByCode(currencyCode).orElseThrow(
                () -> new ApplicationGenericException(HttpStatus.NOT_FOUND,
                        String.format("Currency with code %s not found.", currencyCode))
        );
    }

    public void validateNegativeAmount(Double amount) {
        if (amount < 0) {
            throw new ApplicationGenericException(HttpStatus.NOT_FOUND, "Convertion amount should not be negative");
        }
    }

}
