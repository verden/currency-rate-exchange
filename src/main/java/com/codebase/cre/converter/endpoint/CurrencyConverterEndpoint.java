package com.codebase.cre.converter.endpoint;

import com.codebase.cre.converter.dto.ConvertRequest;
import com.codebase.cre.converter.dto.ConvertResponse;
import com.codebase.cre.converter.service.CurrencyConverterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author William Arustamyan
 */

@RestController
@RequestMapping("/api/v1/converter")
public class CurrencyConverterEndpoint {

    private final CurrencyConverterService converterService;

    public CurrencyConverterEndpoint(CurrencyConverterService converterService) {
        this.converterService = converterService;
    }


    @Operation(summary = "Exchange rate converter")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Exchange conversion successfully ended",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ConvertResponse.class))}),
            @ApiResponse(responseCode = "404", description = "In case if specified currency not found or rate not present",
                    content = @Content)})
    @PostMapping
    public ConvertResponse convert(@Valid @RequestBody ConvertRequest request) {
        return converterService.convert(request);
    }
}
