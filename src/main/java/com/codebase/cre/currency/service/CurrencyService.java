package com.codebase.cre.currency.service;

import com.codebase.cre.currency.dto.CurrencyResponse;
import com.codebase.cre.currency.mapper.CurrencyToResponseMapper;
import com.codebase.cre.currency.repository.CurrencyRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author William Arustamyan
 */

@Service
@Transactional(readOnly = true)
public class CurrencyService {

    private final CurrencyRepository currencyRepository;
    private final CurrencyToResponseMapper currencyToResponseMapper;

    public CurrencyService(CurrencyRepository currencyRepository,
                           CurrencyToResponseMapper currencyToResponseMapper) {
        this.currencyRepository = currencyRepository;
        this.currencyToResponseMapper = currencyToResponseMapper;
    }

    public List<CurrencyResponse> currencies() {
        return currencyRepository.findAll().stream()
                .map(currencyToResponseMapper)
                .collect(Collectors.toList());
    }
}
