package com.codebase.cre.currency.endpoint;

import com.codebase.cre.currency.dto.CurrencyResponse;
import com.codebase.cre.currency.service.CurrencyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author William Arustamyan
 */

@RestController
@RequestMapping("/api/v1/currencies")
public class CurrencyEndpoint {

    private final CurrencyService currencyService;

    public CurrencyEndpoint(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @Operation(summary = "Return list of existing currencies in application")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Return currencies with code and name",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CurrencyResponse.class))})})
    @GetMapping
    public List<CurrencyResponse> currencies() {
        return currencyService.currencies();
    }
}
