package com.codebase.cre.currency.entity;

import lombok.*;

import javax.persistence.*;

/**
 * @author William Arustamyan
 */



@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Builder
@Entity
@Table(name = "t_currencies",
        uniqueConstraints = @UniqueConstraint(columnNames = {"code", "name"}))
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @EqualsAndHashCode.Include
    @Column(name = "code", length = 3, nullable = false)
    private String code;

    @EqualsAndHashCode.Include
    @Column(name = "name")
    private String name;
}
