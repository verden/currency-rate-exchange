package com.codebase.cre.currency.repository;

import com.codebase.cre.currency.entity.Currency;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author William Arustamyan
 */

public interface CurrencyRepository extends JpaRepository<Currency, Long> {

    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = QueryHints.CACHEABLE, value = "true")})
    Optional<Currency> findByCode(String code);

    Set<Currency> findAllByCodeIn(Set<String> codes);

    default Currency baseCurrency() {
        return findByCode("EUR").get();
    }

    @Query("SELECT c.id FROM Currency c")
    List<Long> allIds();
}
