package com.codebase.cre.currency.dto;

import lombok.Data;

/**
 * @author William Arustamyan
 */

@Data
public class CurrencyResponse {

    private Long id;

    private String code;

    private String name;
}
