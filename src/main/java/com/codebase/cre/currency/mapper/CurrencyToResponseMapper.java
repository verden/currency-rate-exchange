package com.codebase.cre.currency.mapper;

import com.codebase.cre.currency.dto.CurrencyResponse;
import com.codebase.cre.currency.entity.Currency;
import org.springframework.stereotype.Component;


import java.util.function.Function;

/**
 * @author William Arustamyan
 */


@Component
public class CurrencyToResponseMapper implements Function<Currency, CurrencyResponse> {

    @Override
    public CurrencyResponse apply(Currency currency) {
        final CurrencyResponse response = new CurrencyResponse();
        response.setId(currency.getId());
        response.setCode(currency.getCode());
        response.setName(currency.getName());
        return response;
    }
}
