package com.codebase.cre.common.config;

import com.codebase.cre.common.config.handler.RestTemplateResponseErrorHandler;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author William Arustamyan
 */

@Configuration
public class ApplicationConfig {

    @Bean
    public RestTemplate createRestTemplate(RestTemplateBuilder templateBuilder) {
        return templateBuilder
                .errorHandler(new RestTemplateResponseErrorHandler())
                .build();
    }
}
