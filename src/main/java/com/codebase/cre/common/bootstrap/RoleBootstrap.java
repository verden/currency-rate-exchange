package com.codebase.cre.common.bootstrap;

import com.codebase.cre.security.entity.Role;
import com.codebase.cre.security.repository.RoleRepository;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author William Arustamyan
 */

@Service
@Order(1)
public final class RoleBootstrap implements ApplicationBootstrap {

    private final RoleRepository roleRepository;

    public RoleBootstrap(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public void init() {
        roleRepository.saveAll(List.of(new Role("ADMIN"), new Role("USER")));
    }

    @Override
    public void destroy() {
        //empty
    }
}
