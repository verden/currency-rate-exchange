package com.codebase.cre.common.bootstrap;

/**
 * @author William Arustamyan
 */

public interface ApplicationBootstrap {

    void init();

    void destroy();
}
