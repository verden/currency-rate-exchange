package com.codebase.cre.common.bootstrap;

import com.codebase.cre.security.entity.Role;
import com.codebase.cre.security.entity.User;
import com.codebase.cre.security.repository.RoleRepository;
import com.codebase.cre.security.repository.UserRepository;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * @author William Arustamyan
 */


@Service
@Order(2)
public final class UserBootstrap implements ApplicationBootstrap {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    public UserBootstrap(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void init() {
        userRepository.save(constructApplicationAdminUser());
    }

    @Override
    public void destroy() {
        //empty
    }

    private User constructApplicationAdminUser() {
        final User user = new User();
        user.setFirstName("CRE Admin");
        user.setLastName("CRE Admin");
        user.setEmail("admin@cre.com");
        Role adminRole = roleRepository.findByName("ADMIN")
                .orElseThrow(() -> new RuntimeException("Admin role is not found."));
        user.setRoles(Set.of(adminRole));
        user.setPassword(passwordEncoder.encode("123"));
        return user;
    }
}
