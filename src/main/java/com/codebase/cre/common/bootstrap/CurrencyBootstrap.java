package com.codebase.cre.common.bootstrap;

import com.codebase.cre.currency.entity.Currency;
import com.codebase.cre.currency.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author William Arustamyan
 */

@Service
@Order(3)
public final class CurrencyBootstrap implements ApplicationBootstrap {

    private final RestTemplate restTemplate;
    private final CurrencyRepository currencyRepository;

    @Value("${cre.currency.provider.url}")
    private String currencyUrl;

    public CurrencyBootstrap(RestTemplate restTemplate, CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
        this.restTemplate = restTemplate;
    }

    @Override
    public void init() {
        final ParameterizedTypeReference<Map<String, String>> responseType = new ParameterizedTypeReference<>() {
        };
        ResponseEntity<Map<String, String>> response =
                restTemplate.exchange(currencyUrl, HttpMethod.GET, HttpEntity.EMPTY, responseType);
        if (response.getBody() == null) {
            throw new IllegalStateException(String.format("Unable to retrieve currencies from url : %s", currencyUrl));
        }
        currencyRepository.saveAll(mapToCurrencies(response.getBody()));
    }

    @Override
    public void destroy() {
        //empty
    }

    private List<Currency> mapToCurrencies(Map<String, String> currencyMap) {
        return currencyMap.entrySet().stream()
                .map(codeName -> new Currency(null, codeName.getKey(), codeName.getValue()))
                .collect(Collectors.toList());
    }
}
