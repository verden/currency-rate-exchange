package com.codebase.cre.common.bootstrap;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PreDestroy;
import java.util.List;

/**
 * @author William Arustamyan
 */

@Service
public final class ApplicationBootstrapRegistry implements CommandLineRunner {

    private final PlatformTransactionManager transactionManager;
    private final List<? extends ApplicationBootstrap> bootstrapRegistry;

    public ApplicationBootstrapRegistry(@Qualifier("transactionManager") PlatformTransactionManager transactionManager, List<? extends ApplicationBootstrap> bootstrapRegistry) {
        this.transactionManager = transactionManager;
        this.bootstrapRegistry = bootstrapRegistry;
    }

    @Override
    public void run(String... args) {
        TransactionTemplate tmpl = new TransactionTemplate(transactionManager);
        tmpl.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                bootstrapRegistry.forEach(ApplicationBootstrap::init);
            }
        });
    }

    @PreDestroy
    public void onExit() {
        TransactionTemplate tmpl = new TransactionTemplate(transactionManager);
        tmpl.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                bootstrapRegistry.forEach(ApplicationBootstrap::destroy);
            }
        });
    }
}
