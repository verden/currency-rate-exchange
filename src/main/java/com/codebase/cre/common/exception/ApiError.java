package com.codebase.cre.common.exception;

import lombok.*;

import java.time.LocalDateTime;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ApiError {

    private String message;
    private Integer status;
    private String error;
    private LocalDateTime timestamp;
    private String exception;
    private String trace;
}
