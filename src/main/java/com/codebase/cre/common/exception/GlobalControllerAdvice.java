package com.codebase.cre.common.exception;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;


@Slf4j
@ControllerAdvice
public class GlobalControllerAdvice extends ResponseEntityExceptionHandler {


    @ExceptionHandler(ApplicationGenericException.class)
    public ResponseEntity<Object> handleGenericResponseStatusException(ApplicationGenericException ex) {
        ApiError apiError = ApiError.builder()
                .message(ex.getReason())
                .status(ex.getStatus()
                        .value())
                .error(ex.getStatus()
                        .getReasonPhrase())
                .timestamp(LocalDateTime.now())
                .build();

        addExceptionDetails(apiError, ex);

        log.info(ExceptionUtils.getStackTrace(ex));

        return new ResponseEntity<>(apiError, ex.getStatus());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex) {
        ApiError apiError = ApiError.builder()
                .message("Invalid access")
                .status(HttpStatus.FORBIDDEN.value())
                .error(HttpStatus.FORBIDDEN.getReasonPhrase())
                .timestamp(LocalDateTime.now())
                .build();

        addExceptionDetails(apiError, ex);

        log.info(ExceptionUtils.getStackTrace(ex));

        return new ResponseEntity<>(apiError, HttpStatus.FORBIDDEN);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {

        ApiError apiError = ApiError.builder()
                .message("Validation failure")
                .status(status.value())
                .error(status.getReasonPhrase())
                .timestamp(LocalDateTime.now())
                .build();

        addExceptionDetails(apiError, ex);

        return new ResponseEntity<>(apiError, status);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handleAllUnhandledExceptions(RuntimeException ex) {
        ApiError apiError = ApiError.builder()
                .message(ex.getMessage())
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .error(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                .timestamp(LocalDateTime.now())
                .build();

        addExceptionDetails(apiError, ex);

        log.error(ExceptionUtils.getStackTrace(ex));

        return new ResponseEntity<>(apiError, HttpStatus.INTERNAL_SERVER_ERROR);
    }


    private void addExceptionDetails(ApiError apiError, Exception ex) {
        apiError.setException(ex.getClass()
                .getName());
        apiError.setTrace(ExceptionUtils.getStackTrace(ex));
    }

}
