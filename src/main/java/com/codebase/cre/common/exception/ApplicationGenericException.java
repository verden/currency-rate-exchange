package com.codebase.cre.common.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;

@Getter
@Setter
public class ApplicationGenericException extends RuntimeException {

    private final HttpStatus status;
    private final String reason;
    private final Throwable cause;

    public ApplicationGenericException(HttpStatus status, @Nullable String reason) {
        this.status = status;
        this.reason = reason;
        this.cause = null;
    }

    public ApplicationGenericException(HttpStatus status, @Nullable String reason, @Nullable Throwable cause) {
        this.status = status;
        this.reason = reason;
        this.cause = cause;
    }
}
