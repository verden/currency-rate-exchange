package com.codebase.cre.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author William Arustamyan
 */

public class CREUser extends User implements Serializable {

    private static final long serialVersionUID = 6529685098267757690L;

    private final Long id;

    public CREUser(String username, String password, Collection<? extends GrantedAuthority> authorities, Long id) {
        super(username, password, authorities);
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
