package com.codebase.cre.security.service;

import com.codebase.cre.common.exception.ApplicationGenericException;
import com.codebase.cre.security.CREUser;
import com.codebase.cre.security.entity.Role;
import com.codebase.cre.security.entity.User;
import com.codebase.cre.security.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author William Arustamyan
 */

@Service("creUserDetailsService")
public class CREUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public CREUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(username)
                .orElseThrow(() -> new ApplicationGenericException(HttpStatus.UNAUTHORIZED,
                        "Invalid username: " + username));
        return new CREUser(user.getEmail(), user.getPassword(), constructGrantedAuthorities(user), user.getId());
    }

    private static Collection<GrantedAuthority> constructGrantedAuthorities(User user) {
        return user.getRoles()
                .stream()
                .map(Role::getName)
                .distinct()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}