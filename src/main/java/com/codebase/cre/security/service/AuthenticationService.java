package com.codebase.cre.security.service;

import com.codebase.cre.common.exception.ApplicationGenericException;
import com.codebase.cre.security.dto.AuthenticationResponse;
import com.codebase.cre.security.dto.AuthenticationTokenRequest;
import com.codebase.cre.security.dto.AuthenticationTokenResponse;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String AUTHORIZATION_HEADER_PREFIX = "Bearer ";

    private final JwtTokenWrapper jwtTokenWrapper;
    private final UserDetailsService mqUserDetailsService;

    public AuthenticationService(JwtTokenWrapper jwtTokenWrapper,
                                 @Qualifier("creUserDetailsService") UserDetailsService mqUserDetailsService) {
        this.jwtTokenWrapper = jwtTokenWrapper;
        this.mqUserDetailsService = mqUserDetailsService;
    }

    public AuthenticationTokenResponse refreshToken(AuthenticationTokenRequest request) {
        String userName = request.getUserName();
        if (jwtTokenWrapper.isValidRefreshToken(request.getRefreshToken())
                && userName.equals(jwtTokenWrapper.getRefreshTokenUserName(request.getRefreshToken()))) {
            String newAccessToken = jwtTokenWrapper.generateAccessToken(userName);
            long expiresAt = jwtTokenWrapper.getAccessTokenExpirationTime(newAccessToken);
            return AuthenticationTokenResponse.builder()
                    .accessToken(newAccessToken)
                    .expiresAt(expiresAt)
                    .build();
        }
        throw new ApplicationGenericException(HttpStatus.UNAUTHORIZED, "Invalid refresh token.");
    }

    public AuthenticationResponse generateTokens(String userName) {
        String accessToken = jwtTokenWrapper.generateAccessToken(userName);
        return AuthenticationResponse.builder()
                .accessToken(accessToken)
                .refreshToken(jwtTokenWrapper.generateRefreshToken(userName))
                .expiresAt(jwtTokenWrapper.getAccessTokenExpirationTime(accessToken))
                .build();
    }

    public void authenticate(String accessToken) {
        if (jwtTokenWrapper.isValidAccessToken(accessToken)) {
            String userName = jwtTokenWrapper.getAccessTokenUserName(accessToken);
            UserDetails userDetails = mqUserDetailsService.loadUserByUsername(userName);
            SecurityContextHolder.getContext()
                    .setAuthentication(new UsernamePasswordAuthenticationToken(userDetails, null,
                            userDetails.getAuthorities()));
        } else if (jwtTokenWrapper.isExpiredAccessToken(accessToken) != null) {
            throw new ApplicationGenericException(HttpStatus.UNAUTHORIZED,
                    jwtTokenWrapper.isExpiredAccessToken(accessToken));
        } else {
            throw new ApplicationGenericException(HttpStatus.UNAUTHORIZED,
                    "You must be authenticated to be able to access this resource.");
        }
    }

}
