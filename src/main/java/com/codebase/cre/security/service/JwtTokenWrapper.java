package com.codebase.cre.security.service;

import com.codebase.cre.common.exception.ApplicationGenericException;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SecurityException;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.time.Instant;
import java.util.Date;

@Component
public class JwtTokenWrapper {

    @Value("${jwt.access-token.secret}")
    private String accessTokenSecretKey;

    @Value("${jwt.access-token.expiration:900000}")
    private Long accessTokenExpirationTime;

    @Value("${jwt.refresh-token.secret}")
    private String refreshTokenSecretKey;

    @Value("${jwt.refresh-token.expiration:3600000}")
    private Long refreshTokenExpirationTime;

    public String generateAccessToken(final String user) {
        return generateToken(user, accessTokenSecretKey, accessTokenExpirationTime);
    }

    public String generateRefreshToken(final String user) {
        return generateToken(user, refreshTokenSecretKey, refreshTokenExpirationTime);
    }

    public String generateToken(String userName, String secretKey, Long expiration) {
        Key key = Keys.hmacShaKeyFor(secretKey.getBytes());
        return Jwts.builder()
                .setSubject(userName)
                .setExpiration(new Date(Instant.now()
                        .plusMillis(expiration)
                        .toEpochMilli()))
                .signWith(key)
                .compact();
    }

    public boolean isValidAccessToken(final String token) {
        return isValidToken(token, accessTokenSecretKey);
    }

    public boolean isValidRefreshToken(final String token) {
        return isValidToken(token, refreshTokenSecretKey);
    }

    private boolean isValidToken(final String token, String secretKey) {
        try {
            parseClaims(token, secretKey);
            return true;
        } catch (SecurityException | ExpiredJwtException | MalformedJwtException | UnsupportedJwtException
                | IllegalArgumentException ex) {
            return false;
        }
    }

    public String isExpiredAccessToken(final String token) {
        return isExpired(token, refreshTokenSecretKey);
    }

    private String isExpired(final String token, String secretKey) {
        try {
            parseClaims(token, secretKey);
        } catch (ExpiredJwtException ex) {
            return ex.getMessage();
        } catch (SecurityException | MalformedJwtException | UnsupportedJwtException | IllegalArgumentException ex) {
            return null;
        }
        return null;
    }

    public String getAccessTokenUserName(String token) {
        return getUsername(token, accessTokenSecretKey);
    }

    public String getRefreshTokenUserName(String token) {
        return getUsername(token, refreshTokenSecretKey);
    }

    private String getUsername(final String token, String secretKey) {
        try {
            return parseClaims(token, secretKey).getBody()
                    .getSubject();
        } catch (Exception ex) {
            throw new ApplicationGenericException(HttpStatus.BAD_REQUEST, "Invalid authentication token.");
        }
    }

    public long getAccessTokenExpirationTime(String token) {
        return getExpirationTime(token, accessTokenSecretKey);
    }

    private long getExpirationTime(String token, String secretKey) {
        return parseClaims(token, secretKey).getBody()
                .getExpiration()
                .toInstant()
                .toEpochMilli();
    }

    private Jws<Claims> parseClaims(String token, String secretKey) throws ExpiredJwtException, UnsupportedJwtException,
            MalformedJwtException, SignatureException, IllegalArgumentException {
        Key key = Keys.hmacShaKeyFor(secretKey.getBytes());
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token);
    }
}
