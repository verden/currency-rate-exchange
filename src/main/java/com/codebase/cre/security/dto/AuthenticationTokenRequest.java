package com.codebase.cre.security.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
public class AuthenticationTokenRequest {
    @NotNull
    private final String userName;

    @NotNull
    private final String refreshToken;
}
