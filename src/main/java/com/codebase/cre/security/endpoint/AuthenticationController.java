package com.codebase.cre.security.endpoint;

import com.codebase.cre.security.dto.AuthenticationTokenRequest;
import com.codebase.cre.security.dto.AuthenticationTokenResponse;
import com.codebase.cre.security.service.AuthenticationService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/refresh-token")
    public AuthenticationTokenResponse refreshToken(
            @Valid @NotNull @RequestBody AuthenticationTokenRequest request) {
        return authenticationService.refreshToken(request);
    }
}
