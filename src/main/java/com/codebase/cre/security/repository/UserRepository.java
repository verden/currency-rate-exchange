package com.codebase.cre.security.repository;

import com.codebase.cre.security.entity.User;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author William Arustamyan
 */

public interface UserRepository extends JpaRepository<User, Long> {
    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = QueryHints.CACHEABLE, value = "true")})
    Optional<User> findByEmail(String email);
}
