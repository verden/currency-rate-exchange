package com.codebase.cre.security.repository;

import com.codebase.cre.security.entity.Role;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author William Arustamyan
 */

public interface RoleRepository extends JpaRepository<Role, Long> {
    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = QueryHints.CACHEABLE, value = "true")})
    Optional<Role> findByName(String roleName);

}
