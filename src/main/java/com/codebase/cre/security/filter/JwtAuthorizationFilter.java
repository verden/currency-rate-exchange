package com.codebase.cre.security.filter;

import com.codebase.cre.security.service.AuthenticationService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.codebase.cre.security.service.AuthenticationService.AUTHORIZATION_HEADER;
import static com.codebase.cre.security.service.AuthenticationService.AUTHORIZATION_HEADER_PREFIX;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private final AuthenticationService authenticationService;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager,
                                  AuthenticationService authenticationService) {
        super(authenticationManager);
        this.authenticationService = authenticationService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        String authorizationHeader = req.getHeader(AUTHORIZATION_HEADER);
        if (authorizationHeader == null || !authorizationHeader.startsWith(AUTHORIZATION_HEADER_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }
        authenticationService.authenticate(authorizationHeader.replace(AUTHORIZATION_HEADER_PREFIX, ""));
        chain.doFilter(req, res);
    }
}
