package com.codebase.cre.security.filter;

import com.codebase.cre.common.exception.ApplicationGenericException;
import com.codebase.cre.security.CREUser;
import com.codebase.cre.security.dto.AuthenticationResponse;
import com.codebase.cre.security.entity.User;
import com.codebase.cre.security.service.AuthenticationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private static final String APPLICATION_LOGIN_PATH = "/api/v1/auth/login";

    private final AuthenticationManager authenticationManager;
    private final AuthenticationService authenticationService;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager,
                                   AuthenticationService authenticationService) {
        this.authenticationManager = authenticationManager;
        this.authenticationService = authenticationService;
        this.setFilterProcessesUrl(APPLICATION_LOGIN_PATH);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) {
        try {
            User user = new ObjectMapper().readValue(req.getInputStream(), User.class);

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword(), new ArrayList<>()));
        } catch (Exception ex) {
            Throwable rootCause = ExceptionUtils.getRootCause(ex);
            String errorMessage;
            if (rootCause instanceof ApplicationGenericException) {
                errorMessage = ((ApplicationGenericException) rootCause).getReason();
            } else {
                errorMessage = ex.getMessage();
            }
            throw new ApplicationGenericException(HttpStatus.UNAUTHORIZED, errorMessage, ex);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
                                            Authentication auth) throws IOException {
        String username = ((CREUser) auth.getPrincipal()).getUsername();

        AuthenticationResponse authResponse = authenticationService.generateTokens(username);
        res.getWriter()
                .write(new ObjectMapper().writeValueAsString(authResponse));
        res.setContentType(APPLICATION_JSON_VALUE);
    }
}
