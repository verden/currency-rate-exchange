package com.codebase.cre.scheduler.apiclient.dto;

import lombok.Data;

import java.util.Map;

/**
 * @author William Arustamyan
 */

@Data
public final class FixerRatesResponse implements RatesResponse {

    private Boolean success;

    private String base;

    private Map<String, Double> rates;

    @Override
    public boolean success() {
        return this.success;
    }

    @Override
    public String baseCode() {
        return this.base;
    }

    @Override
    public Map<String, Double> rates() {
        return this.rates;
    }
}
