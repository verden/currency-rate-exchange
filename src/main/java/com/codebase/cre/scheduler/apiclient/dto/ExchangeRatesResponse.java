package com.codebase.cre.scheduler.apiclient.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

/**
 * @author William Arustamyan
 */

@Data
public final class ExchangeRatesResponse implements RatesResponse {

    private String result;

    @JsonProperty(value = "base_code")
    private String baseCode;

    @JsonProperty(value = "conversion_rates")
    private Map<String, Double> conversionRates;

    @Override
    public boolean success() {
        return this.result.equals("success");
    }

    @Override
    public String baseCode() {
        return baseCode;
    }

    @Override
    public Map<String, Double> rates() {
        return conversionRates;
    }
}
