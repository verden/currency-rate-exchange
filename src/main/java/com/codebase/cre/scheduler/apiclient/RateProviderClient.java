package com.codebase.cre.scheduler.apiclient;

import com.codebase.cre.scheduler.apiclient.dto.RatesResponse;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;

/**
 * @author William Arustamyan
 */

public interface RateProviderClient<Response extends RatesResponse> {

    @Retryable(maxAttempts = 2, value = RuntimeException.class, backoff = @Backoff(delay = 10000, multiplier = 2))
    Response retrieveRates();
}
