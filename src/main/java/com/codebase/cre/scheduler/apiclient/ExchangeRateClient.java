package com.codebase.cre.scheduler.apiclient;

import com.codebase.cre.scheduler.apiclient.dto.ExchangeRatesResponse;
import com.codebase.cre.scheduler.apiclient.dto.RatesResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author William Arustamyan
 */

@Service("exchangeRateProvider")
public class ExchangeRateClient implements RateProviderClient<RatesResponse> {

    @Value("${cre.provider.exchange-rate.url}")
    private String exchangeRateApiUrl;

    @Value("${cre.base.currency.code}")
    private String baseCurrencyCode;

    private final RestTemplate restTemplate;

    public ExchangeRateClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public RatesResponse retrieveRates() {
        ExchangeRatesResponse response = restTemplate.getForObject(exchangeRateApiUrl + baseCurrencyCode, ExchangeRatesResponse.class);
        if (response == null || !response.getResult().equals("success")) {
            //Hint for retry to execute one more time
            throw new RuntimeException("Unable to retrieve rates from Exchange Rate provider");
        }
        return response;
    }
}
