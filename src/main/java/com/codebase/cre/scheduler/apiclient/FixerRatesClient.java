package com.codebase.cre.scheduler.apiclient;

import com.codebase.cre.scheduler.apiclient.dto.FixerRatesResponse;
import com.codebase.cre.scheduler.apiclient.dto.RatesResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author William Arustamyan
 */

@Service("fixerRatesProvider")
public class FixerRatesClient implements RateProviderClient<RatesResponse> {

    @Value("${cre.provider.fixer.url}")
    private String fixerApiUrl;

    private final RestTemplate restTemplate;

    public FixerRatesClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public RatesResponse retrieveRates() {
        FixerRatesResponse response = restTemplate.getForObject(fixerApiUrl, FixerRatesResponse.class);
        if (response == null || !response.getSuccess()) {
            //Hint for retry to execute one more time
            throw new RuntimeException("Unable to retrieve rates from Fixer provider");
        }
        return response;
    }
}
