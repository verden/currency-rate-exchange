package com.codebase.cre.scheduler.apiclient.dto;

import java.util.Map;

/**
 * @author William Arustamyan
 */

public interface RatesResponse {

    boolean success();

    String baseCode();

    Map<String, Double> rates();

}
