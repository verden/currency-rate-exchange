package com.codebase.cre.scheduler.apiclient;

import com.codebase.cre.currency.entity.Currency;
import com.codebase.cre.currency.repository.CurrencyRepository;
import com.codebase.cre.currencyrate.dto.CurrencyRateCreateRequest;
import com.codebase.cre.scheduler.apiclient.dto.RatesResponse;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author William Arustamyan
 */
@Component
public class ApplicationRateClient {

    private final CurrencyRepository currencyRepository;
    private final RateProviderClient<RatesResponse> fixerRatesProvider;
    private final RateProviderClient<RatesResponse> exchangeRateProvider;

    public ApplicationRateClient(CurrencyRepository currencyRepository,
                                 @Qualifier("fixerRatesProvider") RateProviderClient<RatesResponse> fixerRatesProvider,
                                 @Qualifier("exchangeRateProvider") RateProviderClient<RatesResponse> exchangeRateProvider) {
        this.currencyRepository = currencyRepository;
        this.fixerRatesProvider = fixerRatesProvider;
        this.exchangeRateProvider = exchangeRateProvider;
    }

    public List<CurrencyRateCreateRequest> retrieveFixerRates() {
        return constructCreateRequests(fixerRatesProvider.retrieveRates());
    }

    public List<CurrencyRateCreateRequest> retrieveExchangeRates() {
        return constructCreateRequests(exchangeRateProvider.retrieveRates());
    }

    private List<CurrencyRateCreateRequest> constructCreateRequests(RatesResponse response) {
        final Set<Currency> currencies = currencyRepository.findAllByCodeIn(response.rates().keySet());
        return currencies.stream()
                .map(setupCurrencyCreateRequest(response.rates()))
                .collect(Collectors.toList());
    }

    private Function<Currency, CurrencyRateCreateRequest> setupCurrencyCreateRequest(Map<String, Double> rates) {
        return c -> CurrencyRateCreateRequest.builder()
                .currencyId(c.getId())
                .rate(rates.get(c.getCode()))
                .build();
    }

}
