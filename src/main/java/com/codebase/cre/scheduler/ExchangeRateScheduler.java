package com.codebase.cre.scheduler;

import com.codebase.cre.currencyrate.service.CurrencyRateService;
import com.codebase.cre.scheduler.apiclient.ApplicationRateClient;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author William Arustamyan
 */

@Service
public class ExchangeRateScheduler {

    private final CurrencyRateService currencyRateService;
    private final ApplicationRateClient applicationRateClient;

    public ExchangeRateScheduler(CurrencyRateService currencyRateService, ApplicationRateClient rateProvider) {
        this.currencyRateService = currencyRateService;
        this.applicationRateClient = rateProvider;
    }

    @Scheduled(initialDelay = 5000, fixedDelayString = "${cre.provider.exchange-rate.fixedDelay:300000}")
    public void executeExchangeRatesTask() {
        currencyRateService.create(applicationRateClient.retrieveExchangeRates());
    }

    @Scheduled(initialDelay = 5000, fixedDelayString = "${cre.provider.fixer.fixedDelay:300000}")
    public void executeFixerRatesTask() {
        currencyRateService.create(applicationRateClient.retrieveFixerRates());
    }
}
