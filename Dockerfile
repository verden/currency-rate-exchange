FROM gradle:5.6-jdk11 as build
COPY . /home/src
WORKDIR /home/src
RUN gradle clean build

FROM openjdk:11-jre-slim
COPY --from=build /home/src/build/libs/currency-rate-exchange-0.0.1.jar /home/app/currency-rate-exchange.jar
COPY docker_init/wait-for-it.sh /home/app/wait-for-it.sh
RUN chmod +x /home/app/wait-for-it.sh
EXPOSE 8080:8080
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=docker", "/home/app/currency-rate-exchange.jar"]
